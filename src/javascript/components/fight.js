import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const firstFighterHealth = firstFighter.health;
    const secondFighterHealth = secondFighter.health;

    function updateHealth(lr, fighter, totalHealth, damage) {
      fighter.health = fighter.health > damage ? fighter.health - damage : 0;
      const indicatorWidth = fighter.health*100/totalHealth;
      
      document.getElementById(`${lr}-fighter-indicator`).style.width = `${indicatorWidth}%`;

      if (fighter.health == 0) {
        fighter == firstFighter ? resolve(secondFighter) : resolve(firstFighter);
      }
    }

    combo(
      () => {
        updateHealth(
          'right',
          secondFighter,
          secondFighterHealth,
          getDamage(firstFighter, secondFighter, true)
        );
      },
      ...controls.PlayerOneCriticalHitCombination
    );

    combo(
      () => {
        updateHealth(
          'left',
          firstFighter,
          firstFighterHealth,
          getDamage(secondFighter, firstFighter, true)
        );
      },
      ...controls.PlayerTwoCriticalHitCombination
    );

    combo(
      () => {
        updateHealth(
          'left',
          firstFighter,
          firstFighterHealth,
          getDamage(secondFighter, firstFighter)
        );
      },
      controls.PlayerTwoAttack,
      controls.PlayerOneBlock
    );

    combo(
      () => {
        updateHealth(
          'right',
          secondFighter,
          secondFighterHealth,
          getDamage(firstFighter, secondFighter)
        );
      },
      controls.PlayerOneAttack,
      controls.PlayerTwoBlock
    );

    combo(
      () => {
        updateHealth(
          'right',
          secondFighter,
          secondFighterHealth,
          getDamage(firstFighter)
        );
      },
      controls.PlayerOneAttack
    );

    combo(
      () => {
        updateHealth(
          'left',
          firstFighter,
          firstFighterHealth,
          getDamage(secondFighter)
        );
      },
      controls.PlayerTwoAttack
    );
  });
}

export function getDamage(attacker, defender, crit = false) {
  // return damage
  let damage = null;

  if (!defender) {
    damage = getHitPower(attacker);
  } else if (crit) {
    damage = getHitPower(attacker, true)
  } else {
    damage = getHitPower(attacker) - getBlockPower(defender);
  }

  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter, crit = false) {
  // return hit power
  const criticalHitChance = getRandomBetween(1, 2);
  const power = crit ? fighter.attack * 2 : fighter.attack * criticalHitChance;

  return power;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = getRandomBetween(1, 2);
  const power = fighter.defense * dodgeChance;

  return power;
}

function getRandomBetween(min, max) {
  return Math.random() * (max - min) + min;
}

function combo(func, ...codes) {
  let pressed = new Set();

  document.addEventListener('keydown', event => {
    pressed.add(event.code);

    for (let code of codes) {
      if (!pressed.has(code)) {
        return;
      }
    }

    event.preventDefault()
    pressed.clear();
    func();
  }, false);

  document.addEventListener('keyup', event => {
    pressed.delete(event.code);
  }, false);
}
